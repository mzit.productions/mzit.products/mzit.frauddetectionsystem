﻿namespace FraudDetectionSystem
{
    partial class FormPro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPro));
            this.textBoxFirstArticle = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxSecondArticle = new System.Windows.Forms.TextBox();
            this.buttonFirstArticle = new System.Windows.Forms.Button();
            this.buttonSecondArticle = new System.Windows.Forms.Button();
            this.buttonRun = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxContextDisplayKeyword_A = new System.Windows.Forms.TextBox();
            this.textBoxTitlePlusKeyKeyword_A = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxTitlePlusKeyKeyword_B = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxContextDisplayKeyword_B = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxCommonKeyword_AB_DisplayKeyword = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxContextDiceAndCosine = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxFirstArticle
            // 
            this.textBoxFirstArticle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFirstArticle.Location = new System.Drawing.Point(173, 15);
            this.textBoxFirstArticle.Multiline = true;
            this.textBoxFirstArticle.Name = "textBoxFirstArticle";
            this.textBoxFirstArticle.ReadOnly = true;
            this.textBoxFirstArticle.Size = new System.Drawing.Size(685, 100);
            this.textBoxFirstArticle.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "فایل اول:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "فایل دوم:";
            // 
            // textBoxSecondArticle
            // 
            this.textBoxSecondArticle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxSecondArticle.Location = new System.Drawing.Point(173, 121);
            this.textBoxSecondArticle.Multiline = true;
            this.textBoxSecondArticle.Name = "textBoxSecondArticle";
            this.textBoxSecondArticle.ReadOnly = true;
            this.textBoxSecondArticle.Size = new System.Drawing.Size(685, 100);
            this.textBoxSecondArticle.TabIndex = 2;
            // 
            // buttonFirstArticle
            // 
            this.buttonFirstArticle.Location = new System.Drawing.Point(15, 50);
            this.buttonFirstArticle.Name = "buttonFirstArticle";
            this.buttonFirstArticle.Size = new System.Drawing.Size(40, 23);
            this.buttonFirstArticle.TabIndex = 4;
            this.buttonFirstArticle.Text = "...";
            this.buttonFirstArticle.UseVisualStyleBackColor = true;
            this.buttonFirstArticle.Click += new System.EventHandler(this.buttonFirstArticle_Click);
            // 
            // buttonSecondArticle
            // 
            this.buttonSecondArticle.Location = new System.Drawing.Point(15, 160);
            this.buttonSecondArticle.Name = "buttonSecondArticle";
            this.buttonSecondArticle.Size = new System.Drawing.Size(40, 23);
            this.buttonSecondArticle.TabIndex = 5;
            this.buttonSecondArticle.Text = "...";
            this.buttonSecondArticle.UseVisualStyleBackColor = true;
            this.buttonSecondArticle.Click += new System.EventHandler(this.buttonSecondArticle_Click);
            // 
            // buttonRun
            // 
            this.buttonRun.Location = new System.Drawing.Point(72, 15);
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(75, 206);
            this.buttonRun.TabIndex = 6;
            this.buttonRun.Text = "اجرا";
            this.buttonRun.UseVisualStyleBackColor = true;
            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBoxTitlePlusKeyKeyword_A);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxContextDisplayKeyword_A);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(15, 257);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(857, 80);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "مقاله اول";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(740, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "کلمات کلیدی Context :";
            // 
            // textBoxContextDisplayKeyword_A
            // 
            this.textBoxContextDisplayKeyword_A.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxContextDisplayKeyword_A.Location = new System.Drawing.Point(14, 22);
            this.textBoxContextDisplayKeyword_A.Name = "textBoxContextDisplayKeyword_A";
            this.textBoxContextDisplayKeyword_A.ReadOnly = true;
            this.textBoxContextDisplayKeyword_A.Size = new System.Drawing.Size(715, 20);
            this.textBoxContextDisplayKeyword_A.TabIndex = 3;
            // 
            // textBoxTitlePlusKeyKeyword_A
            // 
            this.textBoxTitlePlusKeyKeyword_A.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTitlePlusKeyKeyword_A.Location = new System.Drawing.Point(14, 52);
            this.textBoxTitlePlusKeyKeyword_A.Name = "textBoxTitlePlusKeyKeyword_A";
            this.textBoxTitlePlusKeyKeyword_A.ReadOnly = true;
            this.textBoxTitlePlusKeyKeyword_A.Size = new System.Drawing.Size(715, 20);
            this.textBoxTitlePlusKeyKeyword_A.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(735, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "کلمات کلیدی Key Title :";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.textBoxTitlePlusKeyKeyword_B);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBoxContextDisplayKeyword_B);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(15, 343);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(857, 80);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "مقاله دوم";
            // 
            // textBoxTitlePlusKeyKeyword_B
            // 
            this.textBoxTitlePlusKeyKeyword_B.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTitlePlusKeyKeyword_B.Location = new System.Drawing.Point(14, 52);
            this.textBoxTitlePlusKeyKeyword_B.Name = "textBoxTitlePlusKeyKeyword_B";
            this.textBoxTitlePlusKeyKeyword_B.ReadOnly = true;
            this.textBoxTitlePlusKeyKeyword_B.Size = new System.Drawing.Size(715, 20);
            this.textBoxTitlePlusKeyKeyword_B.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(735, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "کلمات کلیدی Key Title :";
            // 
            // textBoxContextDisplayKeyword_B
            // 
            this.textBoxContextDisplayKeyword_B.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxContextDisplayKeyword_B.Location = new System.Drawing.Point(14, 22);
            this.textBoxContextDisplayKeyword_B.Name = "textBoxContextDisplayKeyword_B";
            this.textBoxContextDisplayKeyword_B.ReadOnly = true;
            this.textBoxContextDisplayKeyword_B.Size = new System.Drawing.Size(715, 20);
            this.textBoxContextDisplayKeyword_B.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(740, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "کلمات کلیدی Context :";
            // 
            // textBoxCommonKeyword_AB_DisplayKeyword
            // 
            this.textBoxCommonKeyword_AB_DisplayKeyword.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxCommonKeyword_AB_DisplayKeyword.Location = new System.Drawing.Point(143, 429);
            this.textBoxCommonKeyword_AB_DisplayKeyword.Name = "textBoxCommonKeyword_AB_DisplayKeyword";
            this.textBoxCommonKeyword_AB_DisplayKeyword.ReadOnly = true;
            this.textBoxCommonKeyword_AB_DisplayKeyword.Size = new System.Drawing.Size(715, 20);
            this.textBoxCommonKeyword_AB_DisplayKeyword.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 432);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(105, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "کلمات کلیدی مشترک :";
            // 
            // textBoxContextDiceAndCosine
            // 
            this.textBoxContextDiceAndCosine.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxContextDiceAndCosine.Location = new System.Drawing.Point(143, 455);
            this.textBoxContextDiceAndCosine.Name = "textBoxContextDiceAndCosine";
            this.textBoxContextDiceAndCosine.ReadOnly = true;
            this.textBoxContextDiceAndCosine.Size = new System.Drawing.Size(715, 20);
            this.textBoxContextDiceAndCosine.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(21, 458);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Dice and Cosine :";
            // 
            // FormPro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 501);
            this.Controls.Add(this.textBoxContextDiceAndCosine);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxCommonKeyword_AB_DisplayKeyword);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonRun);
            this.Controls.Add(this.buttonSecondArticle);
            this.Controls.Add(this.buttonFirstArticle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxSecondArticle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxFirstArticle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormPro";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "سیستم کشف تقلب";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxFirstArticle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxSecondArticle;
        private System.Windows.Forms.Button buttonFirstArticle;
        private System.Windows.Forms.Button buttonSecondArticle;
        private System.Windows.Forms.Button buttonRun;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxContextDisplayKeyword_A;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxTitlePlusKeyKeyword_A;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBoxTitlePlusKeyKeyword_B;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxContextDisplayKeyword_B;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxCommonKeyword_AB_DisplayKeyword;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxContextDiceAndCosine;
        private System.Windows.Forms.Label label8;
    }
}