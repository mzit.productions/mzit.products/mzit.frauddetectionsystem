﻿using FraudDetectionSystem.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FraudDetectionSystem
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        #region Feilds

        /// <summary>
        /// درصورت فعال بودن، مستقیم فایل های داخل پوشه‌ی مقاله را فراخوانی میکند!
        /// در غیر این صورت، 2 بار متوالی، پنجره‌ای باز میشود و مسیر فایلی متنی را دریافت میکند!
        /// در صورتی که تعداد فایل متنی حداقل 2 باشد، مقایسه را با همان 2 مقاله انجام میدهد؛ وگرنه مجددا به پوشه‌ی اصلی مقاله مراجعه میکند!
        /// </summary>
        private bool IsOpenMainArticlesPath = false;

        private List<Article> articles;
        private List<ArticleCompare> articleCompares;

        #endregion

        #region Methods

        /// <summary>
        /// آغاز فرآیند مقایسه ی مقاله ها
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRun_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> txtInputFiles = null;

                if (!IsOpenMainArticlesPath)
                {
                    txtInputFiles = new List<string>();

                    txtInputFiles.Add(Models.BrowseNewFile("*.txt", "First"));
                    txtInputFiles.Add(Models.BrowseNewFile("*.txt", "Second"));
                }

                articles = Models.GetArticles(txtInputFiles);

                articleCompares = Models.GetArticleCompares(articles);
                dataGridViewResult.DataSource = articleCompares;

                dataGridViewResult.Columns["IsValidTwoArticles"].Visible = false;

                dataGridViewResult.Columns["TextKeyword_A"].Visible = false;
                dataGridViewResult.Columns["TitlePlusKeyKeyword_A"].Visible = false;

                dataGridViewResult.Columns["TextKeyword_B"].Visible = false;
                dataGridViewResult.Columns["TitlePlusKeyKeyword_B"].Visible = false;

                dataGridViewResult.Columns["TextDiceAndCosine"].Visible = false;
                dataGridViewResult.Columns["TitlePlusKeyDiceAndCosine"].Visible = false;

                MessageBox.Show("انجام فرآیند به اتمام رسید!");
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        #endregion

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
    }
}
