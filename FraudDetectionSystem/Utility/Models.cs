﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace FraudDetectionSystem.Utility
{
    public class Models
    {
        #region Feilds

        private static List<string> wrongKeys;
        private static string delimiters = "،,.|: |  ";
        private static List<string> delimitersPlus;
        private const char seperatorText = ' ';
        private const char seperatorKeyword = '،';

        private const int titleRatio = 3;
        private const int keywordRatio = 2;
        private const int textRatio = 1;

        #endregion

        #region Helpers

        /// <summary>
        /// Browse File with special details
        /// </summary>
        /// <param name="validInput"></param>
        /// <param name="fromFormat"></param>
        /// <param name="toFormat"></param>
        public static string BrowseNewFile(
            string validInput,
            string title)
        {
            string inputFileName = string.Empty;

            try
            {
                OpenFileDialog openfile = new OpenFileDialog();

                openfile.DefaultExt = validInput;
                openfile.Filter =
                    string.Format(
                        "Browse Object File ({0})|{0}|All File (*.*)|*.*",
                        validInput);
                openfile.Title =
                    string.Format(
                        "Select File For {0} Article!",
                        title);

                var browsefile = openfile.ShowDialog();

                if (browsefile == DialogResult.Yes
                    || browsefile == DialogResult.OK)
                {
                    inputFileName = openfile.FileName;

                    return inputFileName;
                }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return string.Empty;
        }

        /// <summary>
        /// تبدیل متن ورودی به متنی فارسی با تغییر کاراکترهای عربی به فارسی
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ToPersian(string input)
        {
            try
            {
                if (input != null)
                {
                    input = input.Replace('ي', 'ی');
                    input = input.Replace('ك', 'ک');
                    input = input.Replace('‌', ' ');
                    input = input.Replace(',', '،');
                }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return input;
        }

        /// <summary>
        /// خواندن کلماتی که به عنوان کلمه کلیدی شناخته نمیشوند
        /// </summary>
        /// <returns></returns>
        public static List<string> GetDelimiters()
        {
            try
            {
                if (delimitersPlus == null || delimitersPlus.Count() == 0)
                {
                    delimitersPlus = new List<string>();

                    foreach (var item in delimiters.ToList())
                    { delimitersPlus.Add(item.ToString()); }
                }

                if (delimitersPlus == null)
                { delimitersPlus = new List<string>(); }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return delimitersPlus;
        }

        /// <summary>
        /// تبدیل متن به آرایه
        /// </summary>
        /// <param name="delimiters"></param>
        /// <param name="text"></param>
        /// <param name="splitSeperator"></param>
        /// <returns></returns>
        public static List<string> MultiExplode(string text, string delimiters, char splitSeperator)
        {
            List<string> multiExplode = new List<string>();

            try
            {
                Regex pattern = new Regex(delimiters);
                text = pattern.Replace(text.Trim(), splitSeperator.ToString());

                multiExplode = text.Split(splitSeperator).Select(i => i.Trim()).ToList();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return multiExplode;
        }

        #endregion

        #region Methods

        /// <summary>
        /// خواندن کلماتی که به عنوان کلمه کلیدی شناخته نمیشوند
        /// </summary>
        /// <returns></returns>
        public static List<string> GetWrongKeys()
        {
            try
            {
                if (wrongKeys == null || wrongKeys.Count() == 0)
                {
                    string wrongKeyPath =
                        string.Format(
                            "{0}\\{1}",
                            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                            "Data\\WrongKey.txt");

                    var list = new List<string>();
                    var fileStream = new FileStream(wrongKeyPath, FileMode.Open, FileAccess.Read);
                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                    {
                        string line;
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            list.Add(line);
                        }
                    }
                    wrongKeys = list.ToList();
                }

                if (wrongKeys == null)
                { wrongKeys = new List<string>(); }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return wrongKeys;
        }

        /// <summary>
        /// پاک کردن یکسری کلمات که به عنوان کلمه کلیدی شناخته نمیشوند
        /// </summary>
        /// <param name="text"></param>
        /// <param name="wrongKey"></param>
        /// <param name="splitSeperator"></param>
        /// <returns></returns>
        public static List<string> DeleteWrongKey(string text, List<string> wrongKeys, char splitSeperator)
        {
            List<string> lines = new List<string>();

            try
            {
                var multiExplodeText = MultiExplode(text, delimiters, splitSeperator);

                lines =
                    multiExplodeText.Where(i => !wrongKeys.Contains(i))
                    .ToList();
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return lines;
        }

        /// <summary>
        /// تبدیل کلاس کلیدواژه به رشته‌ای به همراه تعداد تکرار جهت نمایش
        /// </summary>
        /// <param name="oKeywords"></param>
        /// <param name="isLite">در صورت فعال بودن، تعداد تکرار نمایش داده نمیشود</param>
        /// <param name="isShowRatio">در صورت فعال بودن، ضریب به جای تعداد تکرار نمایش داده میشود</param>
        /// <returns></returns>
        private static string KeywordsToDisplayKeywords(List<Keyword> oKeywords, bool isLite = false, bool isShowRatio = false)
        {
            string displayKeywords = string.Empty;

            try
            {
                if (oKeywords == null) { return string.Empty; }

                if (isLite)
                {
                    for (int i = 0; i < oKeywords.Count; i++)
                    {
                        displayKeywords +=
                            string.Format(
                                "({0}) ",
                                oKeywords[i].Context);
                    }
                }
                else if (isShowRatio)
                {
                    for (int i = 0; i < oKeywords.Count; i++)
                    {
                        displayKeywords +=
                            string.Format(
                                "({0}={1}) ",
                                oKeywords[i].Ratio,
                                oKeywords[i].Context);
                    }
                }
                else
                {
                    for (int i = 0; i < oKeywords.Count; i++)
                    {
                        displayKeywords +=
                            string.Format(
                                "({0}={1}) ",
                                oKeywords[i].Count,
                                oKeywords[i].Context);
                    }
                }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return displayKeywords;
        }

        /// <summary>
        /// محاسبه ماکسیمم تکرار کلمه و بدست آوردن 20 درصد آن مقدار برای محاسبه کلمات کلیدی
        /// </summary>
        /// <param name="text"></param>
        /// <param name="ratio"></param>
        /// <param name="splitSeperator"></param>
        /// <returns></returns>
        private static List<Keyword> GetKeywords(string text, int ratio, char splitSeperator)
        {
            try
            {
                /// محاسبه کلیدوازه های متن مقاله ورودی
                var listText = DeleteWrongKey(text, GetWrongKeys(), splitSeperator);

                // To just get a sequence
                var countsText =
                    listText.GroupBy(x => x.Trim())
                    .Select(g => new Keyword { Context = g.Key.Trim(), Count = g.Count(), Ratio = ratio })
                    .ToList();

                if (countsText == null)
                { return null; }

                for (int index = 0; index < countsText.Count; index++)
                {
                    countsText[index].Count =
                        listText
                        .Where(i => i.Contains(countsText[index].Context))
                        .Count();
                }

                countsText =
                    countsText
                    .Where(i => (i.Context.Trim().Length > 1))
                    .ToList();

                /// محاسبه کلیدوازه های نهایی
                var maxRepeat = countsText.Max(i => i.Count);

                var keywords =
                    countsText
                    .Where(i => (i.Count >= (maxRepeat / 5)) && (i.Context.Trim().Length > 1))
                    .ToList();

                return keywords;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return null;
        }

        /// <summary>
        /// محاسبه ماکسیمم تکرار کلمه و بدست آوردن 20 درصد آن مقدار برای محاسبه کلمات کلیدی
        /// </summary>
        /// <param name="text"></param>
        /// <param name="ratio"></param>
        /// <returns></returns>
        private static List<Keyword> GetKeywordsPlus(string text, int ratio)
        {
            try
            {
                /// محاسبه کلیدوازه های متن مقاله ورودی
                var listText = DeleteWrongKey(text, GetWrongKeys(), seperatorText);
                /// محاسبه کلیدوازه های کلید اصلی مقاله ورودی
                var listTextPlus = DeleteWrongKey(text, GetWrongKeys(), seperatorKeyword);

                listTextPlus =
                    listTextPlus
                    .Where(i => !listText.Contains(i))
                    .ToList();

                List<string> listMultiText = new List<string>();
                listMultiText.AddRange(listText);

                if (listTextPlus != null && listTextPlus.Count > 0)
                { listMultiText.AddRange(listTextPlus); }

                // To just get a sequence
                var countsText =
                    listMultiText.GroupBy(x => x.Trim())
                    .Select(g => new Keyword { Context = g.Key.Trim(), Count = g.Count(), Ratio = ratio })
                    .ToList();

                if (countsText == null)
                { return null; }

                for (int index = 0; index < countsText.Count; index++)
                {
                    countsText[index].Count =
                        listText
                        .Where(i => i.Contains(countsText[index].Context))
                        .Count();
                }

                countsText =
                    countsText
                    .Where(i => (i.Context.Trim().Length > 1))
                    .ToList();

                /// محاسبه کلیدوازه های نهایی
                var maxRepeat = countsText.Max(i => i.Count);

                var keywords =
                    countsText
                    .Where(i => (i.Count >= (maxRepeat / 5)))
                    .ToList();

                return keywords;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return null;
        }

        /// <summary>
        /// محاسبه ماکسیمم تکرار کلمه و بدست آوردن 20 درصد آن مقدار برای محاسبه کلمات کلیدی
        /// </summary>
        /// <param name="oArticle"></param>
        /// <returns></returns>
        private static List<Keyword> GetKeywords_TitlePlusKeyword(Article oArticle)
        {
            try
            {
                if (oArticle == null) { return null; }

                List<Keyword> countsMultiKeywords = new List<Keyword>();
                countsMultiKeywords.AddRange(oArticle.Title_Keyword);
                countsMultiKeywords.AddRange(oArticle.Key_Keyword);

                countsMultiKeywords =
                    countsMultiKeywords
                    .GroupBy(i => i.Context.Trim())
                    .Select(g => new Keyword { Context = g.Key.Trim(), Count = g.Sum(i => i.Count), Ratio = g.Sum(i => (i.Count * i.Ratio)) })
                    .ToList();

                if (countsMultiKeywords == null)
                { return null; }

                for (int index = 0; index < countsMultiKeywords.Count; index++)
                {
                    countsMultiKeywords[index].Count =
                        countsMultiKeywords.Select(ii => ii.Context)
                        .Where(i => i.Contains(countsMultiKeywords[index].Context))
                        .Count();
                }

                countsMultiKeywords =
                    countsMultiKeywords
                    .Where(i => (i.Context.Trim().Length > 1))
                    .ToList();

                /// محاسبه کلیدوازه های نهایی
                var maxRepeat = countsMultiKeywords.Max(i => (/*i.Count **/ i.Ratio));

                var keywords =
                    countsMultiKeywords
                    .Where(i => ((/*i.Count **/ i.Ratio) >= (maxRepeat / 5)) && (i.Context.Trim().Length > 1))
                    .ToList();

                return keywords;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return null;
        }

        /// <summary>
        /// محاسبه ماکسیمم تکرار کلمه و بدست آوردن 20 درصد آن مقدار برای محاسبه کلمات کلیدی
        /// </summary>
        /// <param name="Article"></param>
        /// <returns></returns>
        private static List<Keyword> GetKeywords_Context(Article oArticle)
        {
            try
            {
                if (oArticle == null) { return null; }

                List<Keyword> countsMultiKeywords = new List<Keyword>();
                countsMultiKeywords.AddRange(oArticle.Title_Keyword);
                countsMultiKeywords.AddRange(oArticle.Text_Keyword);
                countsMultiKeywords.AddRange(oArticle.Key_Keyword);

                countsMultiKeywords =
                    countsMultiKeywords
                    .GroupBy(i => i.Context.Trim())
                    .Select(g => new Keyword { Context = g.Key.Trim(), Count = g.Sum(i => i.Count), Ratio = g.Sum(i => (i.Count * i.Ratio)) })
                    .ToList();

                if (countsMultiKeywords == null)
                { return null; }

                for (int index = 0; index < countsMultiKeywords.Count; index++)
                {
                    countsMultiKeywords[index].Count =
                        countsMultiKeywords.Select(ii => ii.Context)
                        .Where(i => i.Contains(countsMultiKeywords[index].Context))
                        .Count();
                }

                countsMultiKeywords =
                    countsMultiKeywords
                    .Where(i => (i.Context.Trim().Length > 1))
                    .ToList();

                /// محاسبه کلیدوازه های نهایی
                var maxRepeat = countsMultiKeywords.Max(i => (/*i.Count **/ i.Ratio));

                var keywords =
                    countsMultiKeywords
                    .Where(i => ((/*i.Count * */i.Ratio) >= (maxRepeat / 5)) && (i.Context.Trim().Length > 1))
                    .ToList();

                return keywords;
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return null;
        }

        /// <summary>
        /// محاسبه ی انگرام یک مقاله با توجه به مقاله‌ی مقابل جهت محاسبه
        /// </summary>
        /// <param name="oArticleCompare"></param>
        /// <param name="firstArticle">در صورت غیر فعال بودن، مقاله‌ی دوم مورد محاسبه قرار میگیرد</param>
        /// <returns></returns>
        public static List<Keyword> GetNgeram(ArticleCompare oArticleCompare, bool firstArticle)
        {
            try
            {
                if (oArticleCompare == null) { return null; }

                if (oArticleCompare.CommonKeyword_AB != null)
                {
                    Regex pattern;

                    if (firstArticle)
                    {
                        oArticleCompare.Ngeram_A = new List<Keyword>();
                        var listText = DeleteWrongKey(oArticleCompare.Context_A, GetWrongKeys(), seperatorText);
                        var textPlus = string.Join(" ", listText.ToArray());

                        for (int indexOne = 0; indexOne < oArticleCompare.CommonKeyword_AB.Count; indexOne++)
                        {
                            #region محاسبه 1 گرام

                            Keyword oKeyword = new Keyword();

                            oKeyword.Context = oArticleCompare.CommonKeyword_AB[indexOne].Context;

                            pattern = new Regex(oKeyword.Context);
                            oKeyword.Count = pattern.Matches(textPlus).Count;

                            oKeyword.Ratio = oArticleCompare.CommonKeyword_AB[indexOne].Ratio;

                            oArticleCompare.Ngeram_A.Add(oKeyword);

                            #endregion

                            #region محاسبه 2 گرام

                            for (int jTwo = 0; jTwo < oArticleCompare.CommonKeyword_AB.Count; jTwo++)
                            {
                                if (jTwo != indexOne)
                                {
                                    oKeyword = new Keyword();

                                    oKeyword.Context =
                                        oArticleCompare.CommonKeyword_AB[indexOne].Context + " " +
                                        oArticleCompare.CommonKeyword_AB[jTwo].Context;

                                    pattern = new Regex(oKeyword.Context);
                                    oKeyword.Count = pattern.Matches(textPlus).Count;

                                    oKeyword.Ratio =
                                         oArticleCompare.CommonKeyword_AB[indexOne].Ratio +
                                         oArticleCompare.CommonKeyword_AB[jTwo].Ratio;

                                    oArticleCompare.Ngeram_A.Add(oKeyword);

                                    #region محاسبه 3 گرام

                                    for (int kThree = 0; kThree < oArticleCompare.CommonKeyword_AB.Count; kThree++)
                                    {
                                        if (kThree != indexOne && kThree != jTwo)
                                        {
                                            oKeyword = new Keyword();

                                            oKeyword.Context =
                                                oArticleCompare.CommonKeyword_AB[indexOne].Context + " " +
                                                oArticleCompare.CommonKeyword_AB[jTwo].Context + " " +
                                                oArticleCompare.CommonKeyword_AB[kThree].Context;

                                            pattern = new Regex(oKeyword.Context);
                                            oKeyword.Count = pattern.Matches(textPlus).Count;

                                            oKeyword.Ratio =
                                                 oArticleCompare.CommonKeyword_AB[indexOne].Ratio +
                                                 oArticleCompare.CommonKeyword_AB[jTwo].Ratio +
                                                 oArticleCompare.CommonKeyword_AB[kThree].Ratio;

                                            oArticleCompare.Ngeram_A.Add(oKeyword);

                                        }
                                    }

                                    #endregion
                                }
                            }

                            #endregion
                        }
                    }
                    else
                    {
                        oArticleCompare.Ngeram_B = new List<Keyword>();
                        var listText = DeleteWrongKey(oArticleCompare.Context_B, GetWrongKeys(), seperatorText);
                        var textPlus = string.Join(" ", listText.ToArray());

                        for (int indexOne = 0; indexOne < oArticleCompare.CommonKeyword_AB.Count; indexOne++)
                        {
                            #region محاسبه 1 گرام

                            Keyword oKeyword = new Keyword();

                            oKeyword.Context = oArticleCompare.CommonKeyword_AB[indexOne].Context;

                            pattern = new Regex(oKeyword.Context);
                            oKeyword.Count = pattern.Matches(textPlus).Count;

                            oKeyword.Ratio = oArticleCompare.CommonKeyword_AB[indexOne].Ratio;

                            oArticleCompare.Ngeram_B.Add(oKeyword);

                            #endregion

                            #region محاسبه 2 گرام

                            for (int jTwo = 0; jTwo < oArticleCompare.CommonKeyword_AB.Count; jTwo++)
                            {
                                if (jTwo != indexOne)
                                {
                                    oKeyword = new Keyword();

                                    oKeyword.Context =
                                        oArticleCompare.CommonKeyword_AB[indexOne].Context + " " +
                                        oArticleCompare.CommonKeyword_AB[jTwo].Context;

                                    pattern = new Regex(oKeyword.Context);
                                    oKeyword.Count = pattern.Matches(textPlus).Count;

                                    oKeyword.Ratio =
                                         oArticleCompare.CommonKeyword_AB[indexOne].Ratio +
                                         oArticleCompare.CommonKeyword_AB[jTwo].Ratio;

                                    oArticleCompare.Ngeram_B.Add(oKeyword);

                                    #region محاسبه 3 گرام

                                    for (int kThree = 0; kThree < oArticleCompare.CommonKeyword_AB.Count; kThree++)
                                    {
                                        if (kThree != indexOne && kThree != jTwo)
                                        {
                                            oKeyword = new Keyword();

                                            oKeyword.Context =
                                                oArticleCompare.CommonKeyword_AB[indexOne].Context + " " +
                                                oArticleCompare.CommonKeyword_AB[jTwo].Context + " " +
                                                oArticleCompare.CommonKeyword_AB[kThree].Context;

                                            pattern = new Regex(oKeyword.Context);
                                            oKeyword.Count = pattern.Matches(textPlus).Count;

                                            oKeyword.Ratio =
                                                 oArticleCompare.CommonKeyword_AB[indexOne].Ratio +
                                                 oArticleCompare.CommonKeyword_AB[jTwo].Ratio +
                                                 oArticleCompare.CommonKeyword_AB[kThree].Ratio;

                                            oArticleCompare.Ngeram_B.Add(oKeyword);

                                        }
                                    }

                                    #endregion
                                }
                            }

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return null;
        }

        /// <summary>
        /// محاسبه ی Dice and Cosine‌ برای دو رشته متن ورودی
        /// </summary>
        /// <param name="firstKeywords"></param>
        /// <param name="secondKeywords"></param>
        /// <returns></returns>
        public static string GetCosine(List<Keyword> firstKeywords, List<Keyword> secondKeywords)
        {
            try
            {
                /// متن اول

                double L1 = 0;
                if (firstKeywords != null)
                { L1 = firstKeywords.Count(); }

                /// متن دوم

                double L2 = 0;
                if (secondKeywords != null)
                { L2 = secondKeywords.Count(); }

                /// متن اول و دوم

                /// محاسبه کلیدوازه های متن مقاله ورودی
                List<Keyword> countsMultiKeywords = new List<Keyword>();
                countsMultiKeywords.AddRange(firstKeywords);
                countsMultiKeywords.AddRange(secondKeywords);

                double L = 0;
                if (countsMultiKeywords != null)
                {
                    // To just get a sequence
                    var listFirstAndSecondTextPlus =
                        countsMultiKeywords.GroupBy(x => x.Context)
                        .Select(g => g.Key)
                        .ToList();

                    L = listFirstAndSecondTextPlus.Count();
                }

                /// محاسبات

                double C = (L1 + L2) - L;
                double dice = ((2 * C) / (L1 + L2));
                double cosine = (C / (Math.Sqrt(L1) * Math.Sqrt(L2)));

                return string.Format(
                    "Dice is: {0}, Cosine is: {1}",
                    dice,
                    cosine);
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return string.Empty;
        }

        /// <summary>
        /// خواندن تمام مقاله های موجود در پوشه‌ی مربوطه
        /// انجام محاسبات مربوط به هر مقاله
        /// </summary>
        /// <param name="txtInputFiles">در صورت خالی بودن، فایلهای داخل پوشه‌ی مقاله را می‌خواند</param>
        /// <returns></returns>
        public static List<Article> GetArticles(List<string> txtInputFiles = null)
        {
            List<Article> articles = new List<Article>();

            try
            {
                List<string> txtFiles = new List<string>();

                if (txtInputFiles == null || txtInputFiles.Count < 2)
                {
                    string articlesPath =
                        string.Format(
                            "{0}\\{1}",
                            Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                            "Maghale");

                    txtFiles = Directory.EnumerateFiles(articlesPath, "*.txt").ToList();
                }
                else { txtFiles = txtInputFiles; }

                foreach (string currentFile in txtFiles)
                {
                    string[] lines;
                    List<string> texts = new List<string>();
                    var list = new List<string>();
                    var fileStream = new FileStream(currentFile, FileMode.Open, FileAccess.Read);
                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                    {
                        string line;
                        while ((line = streamReader.ReadLine()) != null)
                        {
                            list.Add(line);
                        }
                    }
                    lines = list.ToArray();

                    if (lines.Count() >= 3)
                    {
                        Article oArticle = new Article();

                        oArticle.DisplayName = Path.GetFileNameWithoutExtension(currentFile);

                        oArticle.Context = ToPersian(string.Join(" ", lines.ToArray()));
                        oArticle.Title = ToPersian(lines[0]);

                        texts.Clear();
                        for (int index = 1; index < lines.Count() - 1; index++)
                        {
                            texts.Add(lines[index]);
                        }
                        oArticle.Text = ToPersian(string.Join(" ", texts.ToArray()));

                        oArticle.Key = ToPersian(lines[lines.Count() - 1]);

                        #region جزئیات محاسباتی مقاله

                        oArticle.Title_Keyword = GetKeywords(oArticle.Title, titleRatio, seperatorText);
                        oArticle.Title_DisplayKeyword = KeywordsToDisplayKeywords(oArticle.Title_Keyword);

                        oArticle.Text_Keyword = GetKeywords(oArticle.Text, textRatio, seperatorText);
                        oArticle.Text_DisplayKeyword = KeywordsToDisplayKeywords(oArticle.Text_Keyword);

                        oArticle.Key_Keyword = GetKeywordsPlus(oArticle.Key, keywordRatio);
                        oArticle.Key_DisplayKeyword = KeywordsToDisplayKeywords(oArticle.Key_Keyword);

                        oArticle.TitlePlusKey_Keyword = GetKeywords_TitlePlusKeyword(oArticle);
                        oArticle.TitlePlusKey_DisplayKeyword = KeywordsToDisplayKeywords(oArticle.TitlePlusKey_Keyword, false, true);

                        oArticle.Context_Keyword = GetKeywords_Context(oArticle);
                        oArticle.Context_DisplayKeyword = KeywordsToDisplayKeywords(oArticle.Context_Keyword, false, true);

                        #endregion

                        articles.Add(oArticle);
                    }
                }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return articles;
        }

        /// <summary>
        /// مقایسه ی دو به دوی مقاله ها
        /// انجام محاسبات مربوط به هر دو مقاله
        /// </summary>
        /// <param name="articles"></param>
        /// <returns></returns>
        public static List<ArticleCompare> GetArticleCompares(List<Article> articles)
        {
            List<ArticleCompare> articleCompares = new List<ArticleCompare>();

            try
            {
                if (articles != null && articles.Count > 0)
                {
                    articles =
                        articles
                        .OrderBy(i => i.DisplayName)
                        .ToList();

                    for (int a = 0; a < articles.Count; a++)
                    {
                        for (int b = a + 1; b < articles.Count; b++)
                        {
                            ArticleCompare oArticleCompare = new ArticleCompare();

                            #region جزئیات مقاله اول

                            oArticleCompare.DisplayName_A = articles[a].DisplayName;

                            oArticleCompare.Context_A = articles[a].Context;
                            oArticleCompare.Title_A = articles[a].Title;
                            oArticleCompare.Text_A = articles[a].Text;
                            oArticleCompare.Key_A = articles[a].Key;
                            oArticleCompare.ContextDisplayKeyword_A = articles[a].Context_DisplayKeyword;
                            oArticleCompare.TitleKeyword_A = articles[a].Title_DisplayKeyword;
                            oArticleCompare.TextKeyword_A = articles[a].Text_DisplayKeyword;
                            oArticleCompare.KeyKeyword_A = articles[a].Key_DisplayKeyword;
                            oArticleCompare.TitlePlusKeyKeyword_A = articles[a].TitlePlusKey_DisplayKeyword;

                            #endregion

                            #region جزئیات مقاله دوم

                            oArticleCompare.DisplayName_B = articles[b].DisplayName;

                            oArticleCompare.Context_B = articles[b].Context;
                            oArticleCompare.Title_B = articles[b].Title;
                            oArticleCompare.Text_B = articles[b].Text;
                            oArticleCompare.Key_B = articles[b].Key;
                            oArticleCompare.ContextDisplayKeyword_B = articles[b].Context_DisplayKeyword;
                            oArticleCompare.TitleKeyword_B = articles[b].Title_DisplayKeyword;
                            oArticleCompare.TextKeyword_B = articles[b].Text_DisplayKeyword;
                            oArticleCompare.KeyKeyword_B = articles[b].Key_DisplayKeyword;
                            oArticleCompare.TitlePlusKeyKeyword_B = articles[b].TitlePlusKey_DisplayKeyword;

                            #endregion

                            #region جزئیات محاسباتی مقاله اول و دوم

                            oArticleCompare.CommonKeyword_AB =
                                articles[a].Context_Keyword
                                .Where(i => ((articles[b].Context_Keyword.Select(x => x.Context).ToList()).Contains(i.Context)))
                                .ToList();
                            oArticleCompare.CommonKeyword_AB_DisplayKeyword = KeywordsToDisplayKeywords(oArticleCompare.CommonKeyword_AB, true);

                            GetNgeram(oArticleCompare, true);
                            oArticleCompare.Ngeram_A_Display = KeywordsToDisplayKeywords(oArticleCompare.Ngeram_A);

                            GetNgeram(oArticleCompare, false);
                            oArticleCompare.Ngeram_B_Display = KeywordsToDisplayKeywords(oArticleCompare.Ngeram_B);

                            oArticleCompare.ContextDiceAndCosine = GetCosine(articles[a].Context_Keyword, articles[b].Context_Keyword);
                            oArticleCompare.TitleDiceAndCosine = GetCosine(articles[a].Title_Keyword, articles[b].Title_Keyword);
                            oArticleCompare.TextDiceAndCosine = GetCosine(articles[a].Text_Keyword, articles[b].Text_Keyword);
                            oArticleCompare.KeyDiceAndCosine = GetCosine(articles[a].Key_Keyword, articles[b].Key_Keyword);
                            oArticleCompare.TitlePlusKeyDiceAndCosine = GetCosine(articles[a].TitlePlusKey_Keyword, articles[b].TitlePlusKey_Keyword);

                            #endregion

                            articleCompares.Add(oArticleCompare);
                        }
                    }
                }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }

            return articleCompares;
        }

        #endregion
    }
}
