﻿using System.Collections.Generic;

namespace FraudDetectionSystem.Utility
{
    public class Article
    {
        #region جزئیات مقاله

        public string DisplayName { get; set; }

        public string Context { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Key { get; set; }

        public List<Keyword> Context_Keyword { get; set; }
        public List<Keyword> Title_Keyword { get; set; }
        public List<Keyword> Text_Keyword { get; set; }
        public List<Keyword> Key_Keyword { get; set; }
        public List<Keyword> TitlePlusKey_Keyword { get; set; }

        public string Context_DisplayKeyword { get; set; }
        public string Title_DisplayKeyword { get; set; }
        public string Text_DisplayKeyword { get; set; }
        public string Key_DisplayKeyword { get; set; }
        public string TitlePlusKey_DisplayKeyword { get; set; }

        #endregion
    }
}
