﻿using System.Collections.Generic;

namespace FraudDetectionSystem.Utility
{
    public class ArticleCompare
    {
        public bool IsValidTwoArticles { get; set; }

        #region جزئیات مقاله اول

        public string DisplayName_A { get; set; }

        public string Context_A { get; set; }
        public string Title_A { get; set; }
        public string Text_A { get; set; }
        public string Key_A { get; set; }

        public string ContextDisplayKeyword_A { get; set; }
        public string TitleKeyword_A { get; set; }
        public string TextKeyword_A { get; set; }
        public string KeyKeyword_A { get; set; }
        public string TitlePlusKeyKeyword_A { get; set; }

        #endregion

        #region جزئیات مقاله دوم

        public string DisplayName_B { get; set; }

        public string Context_B { get; set; }
        public string Title_B { get; set; }
        public string Text_B { get; set; }
        public string Key_B { get; set; }

        public string ContextDisplayKeyword_B { get; set; }
        public string TitleKeyword_B { get; set; }
        public string TextKeyword_B { get; set; }
        public string KeyKeyword_B { get; set; }
        public string TitlePlusKeyKeyword_B { get; set; }

        #endregion

        #region جزئیات محاسباتی مقاله اول و دوم

        public List<Keyword> CommonKeyword_AB { get; set; }
        public string CommonKeyword_AB_DisplayKeyword { get; set; }

        public List<Keyword> Ngeram_A { get; set; }
        public string Ngeram_A_Display { get; set; }
        public List<Keyword> Ngeram_B { get; set; }
        public string Ngeram_B_Display { get; set; }

        public string ContextDiceAndCosine { get; set; }
        public string TitleDiceAndCosine { get; set; }
        public string TextDiceAndCosine { get; set; }
        public string KeyDiceAndCosine { get; set; }
        public string TitlePlusKeyDiceAndCosine { get; set; }

        #endregion
    }
}
