﻿namespace FraudDetectionSystem.Utility
{
    public class Keyword
    {
        public string Context { get; set; }
        public int Count { get; set; }
        public int Ratio { get; set; }
    }
}
