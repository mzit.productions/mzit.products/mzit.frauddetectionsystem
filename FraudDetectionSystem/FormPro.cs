﻿using FraudDetectionSystem.Utility;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace FraudDetectionSystem
{
    public partial class FormPro : Form
    {
        public FormPro()
        {
            InitializeComponent();
        }

        #region Feilds

        /// <summary>
        /// درصورت فعال بودن، مستقیم فایل های داخل پوشه‌ی مقاله را فراخوانی میکند!
        /// در غیر این صورت، 2 بار متوالی، پنجره‌ای باز میشود و مسیر فایلی متنی را دریافت میکند!
        /// در صورتی که تعداد فایل متنی حداقل 2 باشد، مقایسه را با همان 2 مقاله انجام میدهد؛ وگرنه مجددا به پوشه‌ی اصلی مقاله مراجعه میکند!
        /// </summary>
        private bool IsOpenMainArticlesPath = false;

        private List<Article> articles;
        private List<ArticleCompare> articleCompares;

        private string firstArticlePath = string.Empty;
        private string secondArticlePath = string.Empty;

        #endregion

        #region Methods

        private void buttonFirstArticle_Click(object sender, EventArgs e)
        {
            try
            {
                firstArticlePath = Models.BrowseNewFile("*.txt", "First");
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void buttonSecondArticle_Click(object sender, EventArgs e)
        {
            try
            {
                secondArticlePath = Models.BrowseNewFile("*.txt", "Second");
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        /// <summary>
        /// آغاز فرآیند مقایسه ی مقاله ها
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRun_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> txtInputFiles = null;

                if (!IsOpenMainArticlesPath)
                {
                    txtInputFiles = new List<string>();

                    txtInputFiles.Add(firstArticlePath);
                    txtInputFiles.Add(secondArticlePath);
                }

                articles = Models.GetArticles(txtInputFiles);

                articleCompares = Models.GetArticleCompares(articles);

                textBoxFirstArticle.Text = articleCompares[0].Context_A;
                textBoxSecondArticle.Text = articleCompares[0].Context_B;

                textBoxContextDisplayKeyword_A.Text = articleCompares[0].ContextDisplayKeyword_A;
                textBoxTitlePlusKeyKeyword_A.Text = articleCompares[0].TitlePlusKeyKeyword_A;

                textBoxContextDisplayKeyword_B.Text = articleCompares[0].ContextDisplayKeyword_B;
                textBoxTitlePlusKeyKeyword_B.Text = articleCompares[0].TitlePlusKeyKeyword_B;

                textBoxCommonKeyword_AB_DisplayKeyword.Text = articleCompares[0].CommonKeyword_AB_DisplayKeyword;
                textBoxContextDiceAndCosine.Text = articleCompares[0].ContextDiceAndCosine;

                MessageBox.Show("انجام فرآیند به اتمام رسید!");
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        #endregion

        private void FormPro_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
    }
}
